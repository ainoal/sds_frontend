# Software development skills - front end

This repository includes two websites created for SDS - front end course. Under "Coursework" directory there is a portfolio website that is made following a tutorial by Traversy Media. Under "Project" directory there is another portfolio site that I designed and coded myself.

In addition, you can find my learning diary and a video of the project in the repository root directory.

## Project: Digital Portfolio Website

The project website uses HTML, CSS (Sass) and plain JavaScript, no libraries or frameworks. The website includes four pages (Home, About me, Projects and Contact me) and a menu.

If you pull my work into your local repository and wish to run my project, the project's index.html file can be found in Project/dist/index.html

You can also find the deployed website via this link: 
<https://ainoal.bitbucket.io/index.html>

## License
[MIT](https://choosealicense.com/licenses/mit/)